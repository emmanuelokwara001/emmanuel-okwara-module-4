import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: (() {}),
        child: Icon(
          Icons.edit,
          color: Colors.white,
        ),
        backgroundColor: Colors.amber[300],
      ),
      appBar: AppBar(
        backgroundColor: Colors.amber[400],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Profile",
              style: TextStyle(color: Colors.white),
            )
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              backgroundImage: AssetImage("assets/cat.jpg"),
              backgroundColor: Colors.amberAccent,
              radius: 50,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                vertical: 50,
                horizontal: 90,
              ),
              child: Column(
                children: [
                  TextField(
                    keyboardType: TextInputType.name,
                    decoration: InputDecoration(
                      hintText: (" " * 6) + "username  🐶🐺",
                    ),
                  ),
                  SizedBox(height: 30),
                  TextField(
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: true,
                    decoration: InputDecoration(
                      hintText: (" " * 6) + "Password  🔑🔐",
                      // icon: Icon(Icons.password),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
